import {ref} from 'vue';

const useIntRandomizer = (minIn = 0, maxIn = 100) => {
  const min = ref(minIn);
  const max = ref(maxIn);
  const range = ref({min: minIn, max: maxIn});
  const result = ref(minIn);
  const count = ref(0);

  function isRequired(x: number | string){
    return x !== '' || 'This value is required';
  }

  function moreThanMin(x: number){
    return x >= min.value || `Must not be smaller than ${min.value}`;
  }

  function lessThanMax(x: number){
    return x <= max.value || `Must not be larger than ${max.value}`;
  }

  function validRange(){
    return range.value.min <= range.value.max || 'Min must not be greater than Max';
  }

  const rules = ref({
    min: [isRequired, moreThanMin, validRange],
    max: [isRequired, lessThanMax, validRange],
  });


  function randomize(){
    result.value = range.value.min + Math.floor(Math.random() * (range.value.max - range.value.min + 1));
    count.value++;
  }

  return {
    min,
    max,
    range,
    rules,
    randomize,
    result,
    count,
  };
}

export default useIntRandomizer;
